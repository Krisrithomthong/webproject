-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for bid_system
CREATE DATABASE IF NOT EXISTS `bid_system` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bid_system`;

-- Dumping structure for table bid_system.user
CREATE TABLE IF NOT EXISTS `user` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Surname` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `IsAdmin` char(50) DEFAULT 'n',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table bid_system.user: ~5 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`UserID`, `Username`, `Password`, `Name`, `Surname`, `Email`, `IsAdmin`) VALUES
	(1, 'Momo1337', '$2y$10$mc/zNxTH1b96QSAGMlmk4uXg9nj3yg7zj0o4hdo3XqRl6ndxQ1uWK', 'Suriya', 'Kretzschmar', 'Sumozerzazz@gmail.com', 'n'),
	(2, 'momo134', '$2y$10$P2Pqex4FQSjyEjOZ.fAOu.nz9pMkvynZCBmzZBbGI3sUnXAV2MTSa', 'Momo', 'Kretzschmar', 'Momo3131@gmail.com', 'n'),
	(3, '1234', '$2y$10$pxdpBBGtbaadd/pxfK2vJuJj08PXJUZmLK0T2qYSSNANBoDVHWWHq', '1234', '1234', '1234', 'n'),
	(4, '123', '$2y$10$2bey06t19oYZK6pwidEykuZQmSUjsV0a2cIBC55KBzvXWUinEsIAm', '123', '123', '123', 'n'),
	(5, '123', '$2y$10$V7uL2D0AtqQ/rRa1ebm9meiaM65P2dPiylYgj531nTv9ox9OU3886', '123', '123', '123', 'n');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

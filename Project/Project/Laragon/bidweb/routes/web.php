<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
use Firebase\JWT\JWT ;  

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post('/register', function(Illuminate\Http\Request $request) {
	
	$name = $request->input("name");
	$surname = $request->input("surname");
	$email = $request->input("email");
	$username = $request->input("username");
	$password = app('hash')->make($request->input("password"));
	
	$query = app('db')->insert('INSERT into user
					(Username, Password, Name, Surname, Email, IsAdmin)
					VALUE (?, ?, ?, ?, ?, ?)',
					[ $username,
					  $password,
					  $name,
					  $surname,
					  $email,
					  'n'] );
	return "Ok";
	
});
$router->post('/login', function(Illuminate\Http\Request $request) {
	
	$username = $request->input("username");
	$password = $request->input("password");
	
	$result = app('db')->select("SELECT UserID, password, IsAdmin FROM user WHERE Username=?",
									[$username]);
									
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "fail";
			$loginResult->reason = "User not found";
	}else {
	
		if(app('hash')->check($password, $result[0]->password)){
			$loginResult->status = "success";
			
			$payload = [
				'iss' => "bid_system",
				'sub' => $result[0]->UserID,
				'iat' => time(),
				'exp' => time()+ 30 * 60 * 60,
			
			];
			
			$loginResult->token = JWT::encode($payload, env('APP_KEY'));
			$loginResult->isAdmin = $result[0]->IsAdmin;
			
		}else {
			$loginResult->status = "fail";
			$loginResult->reason = "Incorrect Password";
		}
	}
	return response()->json($loginResult);
});
$router->get('/get_user_profile', ['middleware'=>'auth', function(Illuminate\Http\Request $request) {

	$user = app('auth')->user();
	$user_id = $user->id;
	
	$results = app('db')->select('SELECT * FROM user
							      WHERE (user.UserID=?)',
										[$user_id]);
	return response()->json($results);
}]);
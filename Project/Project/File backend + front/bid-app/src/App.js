
import './App.css';
import  SignUp from './Signup';
import  Homepage from './Homepage';
import  Mainpage from './mainpage';
import Additem from './Additem';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
	
    <div className="App">
		<Router>
    <Route exact path="/">
					<Homepage />
				 </Route>
			<Switch>
				 <Route path="/Signup">
					<SignUp />
				</Route>
        <Route path="/mainpage">
					<Mainpage />
				</Route>
        <Route path="/Additem">
					<Additem />
				</Route>
			</Switch>
		</Router>
    </div>
  );
}

export default App;

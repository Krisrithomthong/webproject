import {
	Link,
	Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  import './homepage.css'
  
  function Homepage() {
	  
	  const [username, setUsername] = useState("");
	  const [password, setPassword] = useState("");
	  const [isLoggedInToMember, setIsLoggedInToMember] = useState(false);
	  const [isLoggedInToAdmin,  setIsLoggedInToAdmin] =  useState(false);
	  
	  
	  useEffect (()=>{
		  //UseEffect จะทำงานเมื่อ Component ถูกโหลดมาตอนครั้งแรก
		  //เราจะใช้ Check ว่า User Login ไว้หรือยัง  ถ้ายัง ให้ Login แต่ถ้า Login แล้ว ให้วิ่งไปหน้า Member Area เลย
		  //alert(sessionStorage.getItem('api_token')==null);
		  
		  if (sessionStorage.getItem('user_api_token')!=null) {
			  setIsLoggedInToMember(false);
		  }
		  
		  if (sessionStorage.getItem('admin_api_token')!=null) {
			  setIsLoggedInToAdmin(false);
		  }
	  
	  }, []);
	  
	  function sendLogin(){	
	  
			  axios.post('http://localhost/api/v1/login',
				  {
					  "username" : username,
					  "password" : password,
				  }
			  ).then (
				  res=> {				
					  if (res.data.status=="success"){
						  //SessionStorage เป็นคำสั่งมาตารฐานของ Javascript สำหรับการเก็บข้อมูลใดๆที่ใช้ชั่วคราว กรณีนี้เราใช้ Session Storage เก็บ api_token					
						  if ( res.data.isAdmin == 'n'){
							  setIsLoggedInToMember(true);
							  sessionStorage.setItem('user_api_token', res.data.token);
						  }else{
							  setIsLoggedInToAdmin(true);
							  sessionStorage.setItem('admin_api_token', res.data.token);
						  }
					  }else {
						  alert ("ล็อคอินไม่สำเร็จ");
					  }
				  }
			  );	
	  }

	  
	  return (
		  <div>
			  
			<form className ="box">
			<h1 className = "logo"> BidThat</h1>
			  <h2> Login </h2>
			  
			  <div className = "info">Username : </div><input type="text" placeholder = "Username" onChange={(e)=>{setUsername(e.target.value)}}/ >
			  <div className = "info">Password : </div><input type="password" placeholder = "Password" onChange={(e)=>{setPassword(e.target.value)}}/ >
	  
			  <input type="submit" value = "Login" onClick={()=>{sendLogin()}} />
			  
			  <div className ="regis"><Link to="/signup">สมัครสมาชิก</Link></div>
			  {isLoggedInToMember && <Redirect to="/mainpage" /> }
			 {isLoggedInToAdmin &&  <Redirect to="/mainpage" /> }
			 </form>
		  </div>
	  
	  );
  }
  
  export default Homepage;
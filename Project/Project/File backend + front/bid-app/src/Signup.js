
import { useState } from 'react';
import axios from 'axios';
import './Signup.css';

import {
	Redirect
} from "react-router-dom";

function SignUp() {

	const [name, 	setName] 	= useState("");
	const [surname, setSurname] = useState("");
	const [email,  	setEmail] 	= useState("");
	const [username,setUsername]= useState("");
	const [pw1,  	setPw1] 	= useState("");
	const [pw2,  	setPw2] 	= useState("");
	
	const [isSuccess, setIsSuccess] = useState(false);
	function sendSignup(){
			
		if (pw1 != pw2) {			
			alert ("Password ไม่ตรงกันครับ กรุณากรอกใหม่");
		}else{
		
			axios.post('http://localhost/api/v1/register',
				{
					"name" : name,
					"surname" : surname,
					"email" : email,
					"username" : username,
					"password" : pw1,
				}
			).then (
				res=> {	
					
					if (res.data == "Ok") {
							alert("สมัคร สำเร็จ");
							setIsSuccess(true)
					}else {
							alert ("เกิดปัญหา สมัครไม่ได้");
							setIsSuccess(false)
					}
				}
			);
				
		}
		
	}
	
	
	return (<div>
			<form className ="box_su">
			<h1 className = "logo1"> BidThat</h1>
			<h2> Register </h2>
	
			<div className = "info">Name : </div><input type="text" placeholder = "Name" value={name} 	onChange={(e)=>{setName(e.target.value)}}	/ > 
			<div className = "info">Surname : </div><input type="text" placeholder = "Surname" value={surname}	onChange={(e)=>{setSurname(e.target.value)}}	/ > 
			<div className = "info">Email :  </div><input type="text" placeholder = "Email" value={email}				onChange={(e)=>{setEmail(e.target.value)}}	/> 
			<div className = "info">Username : </div><input type="text"  placeholder = "Username" value={username}			onChange={(e)=>{setUsername(e.target.value)}}	/ >
			<div className = "info">Password : </div><input type="password"  placeholder = "Password" value={pw1}			onChange={(e)=>{setPw1(e.target.value)}}	/ > 
			<div className = "info">Confirm Password : </div><input type="password" placeholder = "Confirm Password" value={pw2}	onChange={(e)=>{setPw2(e.target.value)}}  / > 
			
			<input type="submit" value = "Register" onClick={()=>sendSignup()} />
			</form>
			{isSuccess && <Redirect to="/" /> }
	</div>);

}

export default SignUp;
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import * as React from 'react';
import axios from 'axios';
import './mainpage.css';

function mainpage() {
	return(
    <body>
    <div className ="all-contains">
	    <div className = "navbar">
			  <div className = "container">
          <a className = "logo" href = "mainpage">Bid<span className = "span2">That</span></a>

            <nav>
                <ul>
                  <li className ="current"><a href = "">Mainpage</a></li>
                  <li><a href = "">Profile</a></li>
                </ul>

            </nav>

			  </div>
      </div>
  </div>
  </body>
	);
}
export default mainpage;